
var http = require('http');
var app =  require('express')();
var routes = require('./app/routes/routes');
var bodyparser = require('body-parser');
var path = require('path');
var cors = require('cors');
var calls = require('./app/callevents/call');

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

var express = require('express');
app.use(cors());
app.use(bodyparser.json());
app.use('/' , express.static(path.join(__dirname , 'dist')));

const swaggerOptions = {
    swaggerDefinition:{
        info:{
            // The title of the API.
            title: 'Caller API',
            // A short description of the API.
            description: "This is caller API documentation",
            // A URL to the Terms of Service for the API.
            termsOfService: "url",
            // The contact information for the exposed API.
            contact: {
                name: 'Amazing Developer',// The identifying name of the contact person/organization.
                url: "http://www.example.com/support",// The URL pointing to the contact information
                email: "support@example.com"// The email address of the contact person/organization
            },
            // An object representing a Server.
            server:[
                {
                    url: "http://localhost:3000/",
                    description: "This is my nodejs server"
                }
            ],
            // The license information for the exposed API.
            license:{
                name: "Node Js", // The license name used for the API.
                url: "https://nodejs.org/en/" // A URL to the license used for the API.
            },
            // The version of the OpenAPI document 
            version: "1.0.0"
        }
    },
    apis: ['app/auth/auth-routes.js']
}
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',swaggerUi.serve,swaggerUi.setup(swaggerDocs));

var server = http.createServer(app);
var io = require('socket.io').listen(server);

// io.on('connection', (socket) => {
//     console.log("Connected succesfully to the socket ...");
//     socket.emit('news', [1,2,3]);
//     socket.on('newnews',(data) => {
//         console.log(data);
//         socket.emit('news', [1,2,3,4,5]);
//     })
// })

app.use('/voipxp',routes);



server.listen('3000',function(){
    console.log("Listening on 3000");
});