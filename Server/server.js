const app = require("./app");

const port = process.env.PORT || 4444;

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  }
  console.info("listening on port -> ", port);
});