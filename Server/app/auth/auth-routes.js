var app =  require('express').Router();

var auth =  require('./auth');

/**
* @swagger
* /voipxp/auth/login/:
*   post:
*       description: Use for login
*       responses:
*           '200':
*               description: A successful response
*/
app.post('/login', auth.login);

/**
* @swagger
* /voipxp/auth/:id/profile/:
*   get:
*       description: Use to get request all profile
*       responses:
*           '200':
*               description: A successful response
*/
app.get('/:id/profile', auth.profile);

module.exports = app;