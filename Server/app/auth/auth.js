var config = require('../config/config');
var fun = require('../utils/index');
const https = require("https");
const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = config.BASE_URL;

var authobj = {
    login : (req, res) => {
        console.log("inside login");
        axios.post(
            BASE_URL + req.body.username + "/profile/LoginToken",{},
            {
                auth : {
                    username: req.body.username,
                    password: req.body.password
                }
            }
        ).then(success => {
            console.log(success);
            let obj = {};
            obj.token = success.data.LoginToken.token.$;  
            res.send(obj);
        }).catch(error => {
            console.log(error);
            res.send(error.data);
        })
    },
    profile : (req, res) => {
        var id =  req.params.id;
        console.log(req.params.id);
        console.log("inside profile");
        console.log(req.headers.authorization);
        axios.get(
            BASE_URL + id + "/profile/",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error.data);
            res.send(error.data);
        }); 
    }
};


module.exports = authobj;
