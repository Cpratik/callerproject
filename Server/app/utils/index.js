const _ = require("lodash");

exports.convertToArray = object => {
  if (object) {
    if (_.isArray(object)) {
      return object;
    }

    return [].concat(object);
  }
  return [];
};

exports.readablearray = (data) => {
  returnarr = [];
  data.forEach(element => {
      let obj = {};
      for(let k in element ) {
          obj[k] = element[k]['$'];
      }
      returnarr.push(obj);
  });
  return returnarr;
}