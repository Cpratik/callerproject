var EDSConnector = require('./eds');



var callobj = {
    start : (io) =>{
        io.on('connection', (socket) => {
            console.log("Connected succesfully to the socket ...");
            var endPoint = "ws://ec2-13-232-29-90.ap-south-1.compute.amazonaws.com:8080/EdsEvents/events";
            var userCredentials = {username:"5128067815@nat.ipitimi.net", password:"$Sdixit@123", applicationid:"clientappname"};
            var edsConnector = new EDSConnector(endPoint, userCredentials, null);
            var loginSuccessListner =  function() {
                console.log("application login success ");
                console.log("*************");
                socket.emit('loginsuccess', {"msg":"Login Successful"})
            };

            var loginFailedListner = function() {
                console.log("application login failed");
                socket.emit('loginsfailed', {"msg":"Login Failed"})
            };

            var initialEventListner = function(msg) {
                console.log(msg);
                console.log("initial login event=" + msg);
                socket.emit('initiallogin', {"msg":msg})
            };

            var callEventListner = function(msg) {
                console.log("call event=" + msg);
                socket.emit("callevent", {msg: msg})
            };

            var hookStatusListner = function(msg) {
                console.log("hook status callback: msg="+msg);
                socket.emit("hook", {msg: msg});
            };
            edsConnector.registerLoginSuccessCallback(loginSuccessListner);
            edsConnector.registerLoginFailureCallback(loginFailedListner);
            edsConnector.registerInitialUpdateCallback(initialEventListner);
            edsConnector.registerCallUpdateCallback(callEventListner);
            edsConnector.registerHookStatusCallback(hookStatusListner);
            edsConnector.loginToEds();



            socket.emit('news', [1,2,3]);
            socket.on('newnews',(data) => {
                console.log(data);
                socket.emit('news', [1,2,3,4,5]);
            })
        })
    }
}

module.exports = callobj;