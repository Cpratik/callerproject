var app =  require('express').Router();
var directories =  require('./directories');

app.get('/:id/directories/enhancedcalllogs', directories.enhancedcalllogs);
app.get('/:id/directories/enterprise', directories.enterprise);
app.get('/:id/directories/calllog', directories.calllog);
app.get('/:id/directories/group', directories.group);
app.get('/:id/directories/personal', directories.personalget);
app.post('/:id/directories/personal', directories.personalpost);
app.get('/:id/directories/customcontact', directories.customcontact);
app.get('/:id/directories/supervisors', directories.supervisors);
app.get('/:id/directories/callcenters', directories.callcenters);
app.get('/:id/directories/enterprisecommon', directories.enterprisecommon);
app.get('/:id/directories/groupcommon', directories.groupcommon);

module.exports = app;