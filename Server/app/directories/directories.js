var config = require('../config/config')
var fun = require('../utils/index');
const https = require("https");
const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = config.BASE_URL;

var jsonobj = {
	"Personal":{
		"@xmlns":{
			"$":"http://schema.broadsoft.com/xsi"
		},
		"startIndex":{
			"$":"1"
		},
		"numberOfRecords":{
			"$":"1"
		},
		"totalAvailableRecords":{
			"$":"1"
		},
		"entry":{
			"name":{
				"$":"bkabk"
			},
			"number":{
				"$":"12345465698"
				}
		}
	}
};
 
var directoriesobj = {
    enhancedcalllogs : (req, res) => {
        console.log("enhancedcalllog");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/EnhancedCallLogs",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            if (success.data) {
                console.log(fun.readablearray(success.data));
                res.send(fun.readablearray(success.data));
            } else {
                res.send([]);
            }
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    enterprise : (req, res) => {
        console.log("enterprise");
        console.log(req.params.id);
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/Enterprise",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            if (success.data.Enterprise.enterpriseDirectory.directoryDetails) {
                console.log(fun.readablearray(success.data.Enterprise.enterpriseDirectory.directoryDetails));
                res.send(fun.readablearray(success.data.Enterprise.enterpriseDirectory.directoryDetails));
            } else {
                res.send([]);
            }
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    calllog : (req, res) => {
        console.log("calllog");
        var id = req.params.id;
        var obj = {
            placed: {},
            missed: {},
            received: {}
        };
        var placed = [];
        var missed = [];
        var received = [];
        axios.get(
            BASE_URL + id + "/directories/CallLogs",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            placed = fun.readablearray(success.data.CallLogs.placed.callLogsEntry);
            missed = fun.readablearray(success.data.CallLogs.missed.callLogsEntry);
            received = fun.readablearray(success.data.CallLogs.received.callLogsEntry);
            obj.placed = placed;
            obj.missed = missed;
            obj.received = placed;
            console.log(placed);
            console.log(success.data);
            res.send(obj);
            if (obj) { 
                res.send(obj);
            } else {
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    group : (req, res) => {
        console.log("group");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/Group",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data.Group.groupDirectory.directoryDetails) {
                console.log(fun.readablearray(success.data.Group.groupDirectory.directoryDetails));
                res.send(fun.readablearray(success.data.Group.groupDirectory.directoryDetails));
            } else {
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    personalget : (req, res) => {
        console.log("personalget");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/Personal",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            if (success.data.Personal.entry) {
                console.log(fun.readablearray(success.data.Personal.entry));
                res.send(fun.readablearray(success.data.Personal.entry));
            } else {
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    personalpost : (req, res) => {
        console.log("personalpost");
        var id = req.params.id;
        var myobj = jsonobj;
        myobj.Personal.entry.name.$ = req.body.name;
        myobj.Personal.entry.number.$ = req.body.number;
        axios.post(
            BASE_URL + id + "/directories/Personal",myobj,
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error.response.data);
            res.send(error.response.data);
        });
    },
    customcontact : (req, res) => {
        console.log("customcontact");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/CustomContact",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else {
                res.send([])
            }

        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    supervisors : (req, res) => {
        console.log("supervisors");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/Supervisors",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data) {
                console.log(success.data);
                res.send(success.data); 
            } else {
                res.send([])
            }
            
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    callcenters : (req, res) => {
        console.log("callcenters");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/CallCenters",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else {
                res.send([])
            }
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    enterprisecommon : (req, res) => {
        console.log("enterprisecommon");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/EnterpriseCommon",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else {
                res.send([])
            }
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    },
    groupcommon : (req, res) => {
        console.log("groupcommon");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/directories/GroupCommon",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else {
                res.send([])
            }
        }).catch(error => {
            console.log(error.response.data.ErrorInfo);
            res.send(error.response.data.ErrorInfo);
        });
    }
}


module.exports = directoriesobj;
