var app =  require('express').Router();
var user = require('./user');

app.get('/:id/profile/device', user.device);
app.get('/:id/profile/devicelineport', user.devicelineport);
app.get('/:id/profile/fac', user.fac);
app.get('/:id/profile/registration', user.registration);
app.get('/version', user.version);

module.exports = app;