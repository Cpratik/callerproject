var config = require('../config/config')
var readablearray = require('../utils/index');
const https = require("https");
const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = config.BASE_URL;

var userobj = {
    device : (req, res) => {
        console.log("device");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/profile/Device",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    devicelineport : (req, res) => {
        console.log("devicelineport");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/profile/Device",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    fac : (req, res) => {
        console.log("fac");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/profile/Fac",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    registration : (req, res) => {
        console.log("registration");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/profile/Registrations",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    version : (req, res) => {
        console.log("version");
        axios.get(
            "http://35.229.84.55/com.broadsoft.xsi-actions/v2.0/versions",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    }
}

module.exports = userobj;