const express = require("express");
const router = new express.Router();
const BroadsoftAPI = require("../api/broadsoft");
const utils = require("../utils");
const moment = require("moment");

router.post("/login" , (req,res) => {
  BroadsoftAPI.login(req.body.userId, req.body.password)
  .then((response) => {
    return res.status(200).send({
      "message": "Valid Profile"
    });
  })
  .catch((error) => {
    return res
      .status(error.response ? error.response.status : 500)
      .send(error.response ? error.response.data : error.code);
  });
});

router.get("/:id/profile", (req, res) => {
  BroadsoftAPI.getProfile(req.params.id, req.headers)
    .then((response) => {
      return res.status(200).send({
        "message": "Valid Profile"
      });
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

router.get("/:id/call-logs", (req, res) => {
  BroadsoftAPI.callLogs(req.params.id, req.headers)
    .then((response) => {
      const calls = getCallLogs(response.data.CallLogs.placed, "PLACED")
        .concat(getCallLogs(response.data.CallLogs.received, "RECEIVED"))
        .concat(getCallLogs(response.data.CallLogs.missed, "MISSED"));
      return res.status(200).send(
        calls.sort(function(a, b) {
          return moment(a.time).isAfter(b.time) ? -1 : 1;
        })
      );
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

router.get("/:id/contacts", (req, res) => {
  BroadsoftAPI.enterpriseContacts(
    req.params.id,
    req.query.start,
    req.query.count,
    req.query.search || "",
    req.headers
  )
    .then((response) => {
      return res.status(200).send(getContacts(response.data.Enterprise));
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

router.get("/:id/call/:number", (req, res) => {
  BroadsoftAPI.initiateCall(req.params.id, req.params.number, req.headers)
    .then((response) => {
      return res.status(200).send({ message: "Call initiated", callId: response.data.CallStartInfo.callId.$ });
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

router.put("/:id/call/:callId", (req, res) => {
  getCallPropertiesUpdateMethod(req)
    .then((response) => {
      return res.status(200).send({ message: "Call property updated successfully" });
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

const getCallPropertiesUpdateMethod = (req) => {
  const settings = req.body;
  switch (Object.keys(settings)[0]) {
    case "hold":
      return BroadsoftAPI.hold(
        req.params.id,
        req.headers,
        settings.hold,
        req.params.callId
      );
    case "record":
      return BroadsoftAPI.record(
        req.params.id,
        req.headers,
        settings.record,
        req.params.callId
      );
    default:
  }
};

router.get("/:id/settings", (req, res) => {
  Promise.all([
    BroadsoftAPI.callForwardingAlways(req.params.id, req.headers),
    BroadsoftAPI.callForwardingBusy(req.params.id, req.headers),
    BroadsoftAPI.callForwardingNoAnswer(req.params.id, req.headers),
    BroadsoftAPI.doNotDisturb(req.params.id, req.headers),
    BroadsoftAPI.remoteOffice(req.params.id, req.headers),
    BroadsoftAPI.broadWorksAnywhere(req.params.id, req.headers),
  ])
    .then((values) => {
      return res.status(200).send({
        callForwardingAlways: getCallForwardingObject(
          "CallForwardingAlways",
          values[0]
        ),
        callForwardingBusy: getCallForwardingObject(
          "CallForwardingBusy",
          values[1]
        ),
        callForwardingNoAnswer: getCallForwardingObject(
          "CallForwardingNoAnswer",
          values[2]
        ),
        doNotDisturb: {
          active:
            (values[3].data &&
              values[3].data.DoNotDisturb.active &&
              values[3].data.DoNotDisturb.active.$) === "true",
        },
        remoteOffice: {
          active:
            (values[4].data &&
              values[4].data.RemoteOffice.active &&
              values[4].data.RemoteOffice.active.$) === "true",
          forwardToPhoneNumber:
            values[4].data &&
            values[4].data.RemoteOffice.remoteOfficeNumber &&
            values[4].data.RemoteOffice.remoteOfficeNumber.$,
        },
        broadWorksAnywhere: {
          active:
            (values[5].data &&
              values[5].data.BroadWorksAnywhere
                .alertAllLocationsForClickToDialCalls &&
              values[5].data.BroadWorksAnywhere
                .alertAllLocationsForClickToDialCalls.$) === "true",
        },
      });
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

router.put("/:id/settings", (req, res) => {
  getSettingsUpdateMethod(req)
    .then((response) => {
      return res.status(200).send({ message: "Settings updated successfully" });
    })
    .catch((error) => {
      return res
        .status(error.response ? error.response.status : 500)
        .send(error.response ? error.response.data : error.code);
    });
});

const getCallForwardingObject = (type, response) => {
  return {
    active:
      (response.data &&
        response.data[type].active &&
        response.data[type].active.$) === "true",
    forwardToPhoneNumber:
      response.data &&
      response.data[type].forwardToPhoneNumber &&
      response.data[type].forwardToPhoneNumber.$,
  };
};

const getSettingsUpdateMethod = (req) => {
  const settings = req.body;
  switch (Object.keys(settings)[0]) {
    case "callForwardingAlways":
      return BroadsoftAPI.setCallForwardingAlways(
        req.params.id,
        req.headers,
        settings.callForwardingAlways
      );
    case "callForwardingBusy":
      return BroadsoftAPI.setCallForwardingBusy(
        req.params.id,
        req.headers,
        settings.callForwardingBusy
      );
    case "callForwardingNoAnswer":
      return BroadsoftAPI.setCallForwardingNoAnswer(
        req.params.id,
        req.headers,
        settings.callForwardingNoAnswer
      );
    case "doNotDisturb":
      return BroadsoftAPI.setDoNotDisturb(
        req.params.id,
        req.headers,
        settings.doNotDisturb
      );
    case "remoteOffice":
      return BroadsoftAPI.setRemoteOffice(
        req.params.id,
        req.headers,
        settings.remoteOffice
      );
    case "broadWorksAnywhere":
      return BroadsoftAPI.setBroadWorksAnywhere(
        req.params.id,
        req.headers,
        settings.broadWorksAnywhere
      );
    default:
  }
};

const getContacts = (contacts) => {
  return {
    contacts: utils
      .convertToArray(
        contacts.enterpriseDirectory &&
          contacts.enterpriseDirectory.directoryDetails
      )
      .map((item) => getContactItem(item)),
    totalAvailableRecords: contacts.totalAvailableRecords.$,
    numberOfRecords: contacts.numberOfRecords.$,
    startIndex: contacts.startIndex.$,
  };
};

const getCallLogs = (logs, type) => {
  return utils.convertToArray(logs.callLogsEntry).map((item) => {
    return { ...getCallLogItem(item), type: type };
  });
};

const getContactItem = (item) => {
  return {
    userId: item.userId && item.userId.$,
    firstName: item.firstName && item.firstName.$,
    lastName: item.lastName && item.lastName.$,
    number: item.number && item.number.$,
    extension: item.extension && item.extension.$,
  };
};

const getCallLogItem = (item) => {
  return {
    countryCode: item.countryCode && item.countryCode.$,
    phoneNumber: item.phoneNumber && item.phoneNumber.$,
    name: item.name && item.name.$,
    time: item.time && new Date(item.time.$),
    callLogId: item.callLogId && item.callLogId.$,
  };
};

module.exports = router;
