const https = require("https");

const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = "https://broadsoftsandboxxsp.cisco.com";

exports.login = (userId, password) =>{
  return axios.post(
    BASE_URL
  );
};

exports.getProfile = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/profile",
    getHeaders(headers)
  );
};


exports.callLogs = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/directories/calllogs",
    getHeaders(headers)
  );
};

exports.enterpriseContacts = (userId, start, count, search, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/directories/enterprise",
    {
      ...getHeaders(headers),
      params: {
        start: start,
        results: count,
        sortColumn: "firstName",
        firstName: `*${search}*`,
        lastName: `*${search}*`,
        number: `*${search}*`,
      },
    }
  );
};

exports.callForwardingAlways = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingalways",
    getHeaders(headers)
  );
};

exports.setCallForwardingAlways = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingalways",
    {
      CallForwardingAlways: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        active: { $: settings.active + "" },
        forwardToPhoneNumber: {
          $: settings.forwardToPhoneNumber,
        },
      },
    },
    getHeaders(headers)
  );
};

exports.initiateCall = (userId, number, headers) => {
  return axios.post(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/calls/new?address=" +
      number,
    null,
    getHeaders(headers)
  );
};

exports.callForwardingBusy = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingbusy",
    getHeaders(headers)
  );
};

exports.setCallForwardingBusy = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingbusy",
    {
      CallForwardingBusy: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        active: { $: settings.active + "" },
        forwardToPhoneNumber: {
          $: settings.forwardToPhoneNumber,
        },
      },
    },
    getHeaders(headers)
  );
};

exports.callForwardingNoAnswer = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingnoanswer",
    getHeaders(headers)
  );
};

exports.setCallForwardingNoAnswer = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/callforwardingnoanswer",
    {
      CallForwardingNoAnswer: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        active: { $: settings.active + "" },
        forwardToPhoneNumber: {
          $: settings.forwardToPhoneNumber,
        },
      },
    },
    getHeaders(headers)
  );
};

exports.doNotDisturb = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/donotdisturb",
    getHeaders(headers)
  );
};

exports.setDoNotDisturb = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/donotdisturb",
    {
      DoNotDisturb: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        active: { $: settings.active + "" },
      },
    },
    getHeaders(headers)
  );
};

exports.remoteOffice = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/remoteoffice",
    getHeaders(headers)
  );
};

exports.setRemoteOffice = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/remoteoffice",
    {
      RemoteOffice: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        active: { $: settings.active + "" },
        remoteOfficeNumber: {
          $: settings.forwardToPhoneNumber,
        },
      },
    },
    getHeaders(headers)
  );
};

exports.broadWorksAnywhere = (userId, headers) => {
  return axios.get(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/broadworksanywhere",
    getHeaders(headers)
  );
};

exports.setBroadWorksAnywhere = (userId, headers, settings) => {
  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/services/broadworksanywhere",
    {
      BroadWorksAnywhere: {
        "@xmlns": { $: "http://schema.broadsoft.com/xsi" },
        alertAllLocationsForClickToDialCalls: { $: settings.active + "" },
      },
    },
    getHeaders(headers)
  );
};

exports.hold = (userId, headers, settings, callId) => {
  let type = "reconnect";
  if (settings.active) {
    type = "hold";
  }

  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/calls/" +
      callId +
      "/" +
      type,
    null,
    getHeaders(headers)
  );
};

exports.record = (userId, headers, settings, callId) => {
  let type = "stopRecording";
  if (settings.active) {
    type = "record";
  }

  return axios.put(
    BASE_URL +
      "/com.broadsoft.xsi-actions/v2.0/user/" +
      userId +
      "/calls/" +
      callId +
      "/" +
      type,
    null,
    getHeaders(headers)
  );
};

const getHeaders = (headers) => {
  return {
    headers: {
      authorization: headers.authorization,
    },
  };
};
