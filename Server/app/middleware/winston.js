const winston = require('winston');
const expressWinston = require('express-winston');

exports.logger = expressWinston.logger({
    transports: [
        new winston.transports.Console({
            silent: false
        })
      ],
      format: winston.format.combine(
        winston.format.json()
      ),
  });

exports.errorLogger = expressWinston.errorLogger({
    transports: [
      new winston.transports.Console({
          silent: false
      })
    ],
    format: winston.format.combine(
      winston.format.json()
    )
  });