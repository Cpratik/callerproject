var app =  require('express').Router();
var authroutes =  require('../auth/auth-routes');
var directoriesroutes = require('../directories/directories-routes');
var servicesroutes = require('../services/services-routes');
var userroutes = require('../user/user-routes');
var callroutes = require('../call/call-routes');

app.use('/auth', authroutes);
app.use('/directories', directoriesroutes);
app.use('/services', servicesroutes)
app.use('/user', userroutes);
app.use('/calls', callroutes);

module.exports = app;