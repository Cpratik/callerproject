var app = require('express').Router();
var callobj = require('./call');

app.post('/:id/calls/new', callobj.new);
app.put('/:id/calls/:callid/hold', callobj.hold);
app.put('/:id/calls/:callid/talk', callobj.talk);

module.exports = app;