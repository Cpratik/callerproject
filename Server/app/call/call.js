var config = require('../config/config')
var fun = require('../utils/index');
const https = require("https");
const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = config.BASE_URL;

var callobj = {
    "new" :  (req, res) => {
        console.log('new');
        var id =  req.params.id;
        console.log(BASE_URL + id + "/calls/new?address=" + req.body.number);
        axios.post(
            BASE_URL + id + "/calls/new?address=" + req.body.number,{},
            {
                headers : { 
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success);
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else { 
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    "hold" : (req, res) => {
        console.log('hold');
        var id = req.params.id;
        var callid = req.params.callid;
        axios.put(
            BASE_URL + id + "/calls/" + callid + "/Hold", {},
            {
                headers : { 
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success);
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else { 
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    "talk" : (req, res) => {
        console.log('talk');
        var id = req.params.id;
        var callid = req.params.callid;
        axios.put(
            BASE_URL + id + "/calls/" + callid + "/Talk", {},
            {
                headers : { 
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success);
            if (success.data) {
                console.log(success.data);
                res.send(success.data);
            } else { 
                res.send([]);
            }
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    }

};


module.exports = callobj;