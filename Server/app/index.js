const express = require("express");
const router = require("./router");
const bodyParser = require("body-parser");
const app = express();
const winston = require("./middleware/winston");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(winston.logger);

app.use("/api", router);

app.use(winston.errorLogger);

module.exports = app;
