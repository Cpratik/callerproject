var config = require('../config/config')
var fun = require('../utils/index');
const https = require("https");
const axios = require("axios").create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});

const BASE_URL = config.BASE_URL;

var serviceobj = {
    callwaiting : (req, res) => {
        console.log("callwaiting");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallWaiting",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    donotdisturb :  (req, res) => {
        console.log("donotdisturb");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/DoNotDisturb",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    services : (req, res) => {
        console.log("services");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(fun.readablearray(success.data.Services.service));
            res.send(fun.readablearray(success.data.Services.service));
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    speeddial8 : (req, res) => {
        console.log("speeddial8");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/SpeedDial8",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    speeddial100 : (req, res) => {
        console.log("speeddial100");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/SpeedDial100",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callcenter : (req, res) => {
        console.log("callcenter");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallCenter",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callcentermonitoring : (req, res) => {
        console.log("callcentermonitoring");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallCenterMonitoring",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingalways : (req, res) => {
        console.log("callforwardingalways");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallForwardingAlways",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingalwaysput : (req, res) => {
        console.log("callforwardingalways");
        var id = req.params.id;
        console.log(BASE_URL+id+ "/services/CallForwardingAlways");
        axios.put(
            BASE_URL + id + "/services/CallForwardingAlways",
            {
                "CallForwardingAlways": {
                    "@xmlns": {
                        "$": "http://schema.broadsoft.com/xsi"
                    },
                    "active": {
                        "$": "true"
                    },
                    "ringSplash": {
                        "$": "true"
                    }
                }
            },
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingalwayssecondary : (req, res) => {
        console.log("callforwardingalwayssecondary");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallForwardingAlwaysSecondary",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingbusy : (req, res) => {
        console.log("callforwardingbusy");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallForwardingBusy",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingnotreachable : (req, res) => {
        console.log("callforwardingnotreachable");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallForwardingNotReachable",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    broadworksanywhere : (req, res) => {
        console.log("broadworksanywhere");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/BroadWorksAnywhere",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    },
    callforwardingnoanswer : (req, res) => {
        console.log("callforwardingnoanswer");
        var id = req.params.id;
        axios.get(
            BASE_URL + id + "/services/CallForwardingNoAnswer",
            {
                headers : {
                    "Authorization" : req.headers.authorization
                }
            }
        ).then(success => {
            console.log(success.data);
            res.send(success.data);
        }).catch(error => {
            console.log(error);
            res.send(error);
        });
    }
};

module.exports = serviceobj;