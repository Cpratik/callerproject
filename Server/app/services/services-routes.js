var app =  require('express').Router();
var services = require('./services');

app.get('/:id/services/callwaiting', services.callwaiting);
app.get('/:id/services/donotdisturb', services.donotdisturb);
app.get('/:id/services', services.services);
app.get('/:id/services/speeddial8', services.speeddial8);
app.get('/:id/services/speeddial100', services.speeddial100);
app.get('/:id/services/callcenter', services.callcenter);
app.get('/:id/services/callcentermonitoring', services.callcentermonitoring);
app.get('/:id/services/callforwardingalways', services.callforwardingalways);
app.get('/:id/services/callforwardingalwayssecondary', services.callforwardingalwayssecondary);
app.get('/:id/services/callforwardingbusy', services.callforwardingbusy);
app.get('/:id/services/callforwardingnotreachable', services.callforwardingnotreachable);
app.get('/:id/services/broadworksanywhere', services.broadworksanywhere);
app.get('/:id/services/callforwardingnoanswer', services.callforwardingnoanswer);

app.put('/:id/services/callforwardingalways', services.callforwardingalwaysput);

module.exports = app;