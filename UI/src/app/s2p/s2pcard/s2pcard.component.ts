import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DirectoriesService } from '../../services/directories.service';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { CalleventsService } from '../../services/callevents.service';
import {CallerService} from '../../caller.service';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-s2pcard',
  templateUrl: './s2pcard.component.html',
  styleUrls: ['./s2pcard.component.css']
})


export class S2pcardComponent implements OnInit {

  constructor(private directorySer: DirectoriesService, private callSer: CalleventsService, private calls: CallerService) {
    this.loginSuccessListner = this.loginSuccessListner.bind(this);
    this.loginFailedListner = this.loginFailedListner.bind(this);
    this.initialEventListner = this.initialEventListner.bind(this);
    this.callEventListner = this.callEventListner.bind(this);
    this.hookStatusListner = this.hookStatusListner.bind(this);
  }
  navbarOpen = false;

  // Personal Form Details
  personalForm = new FormGroup({
    name: new FormControl('', Validators.required),
    contactnumber: new FormControl('', Validators.required)
  });

  // Dial Form
  dialForm = new FormGroup({
    dialnum : new FormControl('', Validators.required),
  });

  forwardForm = new FormGroup({

  });

  settingForm = new FormGroup({});

  keys: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#' ];

  // left tables

  // current calls table
  current: any = [];
  selectedActiveCall: any = {};
  selectedIndex = 0;
  // parked calls table
  parked: any = [ ];

  // conferences table
  conferences: any = [ ];

  // right tables

  // groups table
  groups: any = [];

  // personal table
  personal: any = [];

  // enterprise table
  enterprise: any = [];

  // call logs table
  callogs: any = [];

  // preferences cards
  preferences: any = [
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
    {
      avatar: '../../../../assets/img/avatars/5.jpg',
      name: 'Marcia Follet',
      info: 'Miracle',
      status: 'Available',
    },
  ];

  temp = '';

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  // getGroup() {
  //   this.directorySer.getgroup().subscribe( success => {
  //     this.groups = success;
  //     console.log(success);
  //   }, error => {
  //     console.log(error);
  //   });
  // }

  getPersonal() {
    this.directorySer.getpersonal().subscribe( success => {
      let obj : any = success;
      if (obj.name != 'Error') {
        this.personal = success;
      }
      // this.personal = success;
      console.log(success);
    }, error => {
      console.log(error);
      this.personal = [];
    });
  }

  postPersonal() {
    this.directorySer.postpersonal(this.personalForm.value).subscribe( success => {
      console.log(success);
      this.getPersonal();
      this.reset();
    }, error => {
      console.log(error);
    });
  }

  reset() {
    this.personalForm.reset();
    this.dialForm.reset();
  }

  getEnterprise() {
    this.directorySer.getenterprise().subscribe( success => {
      this.enterprise = success;
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  getCalllog() {
    this.directorySer.getcalllogs().subscribe( success => {
      this.callogs = success;
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  edslogin() {
    this.callSer.login().subscribe( success => {
      console.log(success);
      this.getCallevents();
    }, error => {
      console.log(error);
    });
  }

  getCallevents() {
    this.callSer.callevents().subscribe( success => {
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  postNew() {
    const num = {
      'number' : this.keys
    };
    this.callSer.new(num).subscribe( success => {
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  appendnum(key) {
    const num = this.dialForm.controls.dialnum.value + key;
    this.dialForm.controls.dialnum.setValue(num);
  }

  slicechar(){
    console.log('inside slice');
    
    var str = this.dialForm.controls.dialnum.value;
    str = str.substring(0, str.length - 1);
    this.dialForm.controls.dialnum.setValue(str);
    console.log(str);
    
  }

  selectActiveCalls(call) {
    this.selectedActiveCall = call;
  }

  putHold() {
    this.callSer.hold(this.selectedActiveCall.callid).subscribe( success => {
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  putTalk() {
    this.callSer.talk(this.selectedActiveCall.callid).subscribe( success => {
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  ngOnInit(): void {

    // this.getGroup();
    this.getPersonal();
    // this.getEnterprise();
    // this.getCalllog();
    this.initiateSocket();
  }

  initiateSocket() {
    // this.calls.login();
    this.calls.registerHandlers(this.loginSuccessListner, this.loginFailedListner, this.initialEventListner,
    this.callEventListner, this.hookStatusListner);
  }

  loginSuccessListner = () => {
    console.log('application login success');
    console.log('*************');
  }

  loginFailedListner = () => {
    console.log('application login failed');
  }

  initialEventListner = (msg) => {

    console.log(msg);
    // this.current = msg.calls.call;
    // console.log(this.current);
    // this.temp = this.current[0].callId;
    // console.log(this.temp);
    this.addAlreadyActiveCalls(msg.calls);
    console.log('initial login event=' + msg);
  }

  dateDiffToString(a, b) {

    // make checks to make sure a and b are not null
    // and that they are date | integers types

    let diff = Math.abs(a - b);

    const ms = diff % 1000;
    diff = (diff - ms) / 1000;
    const ss = diff % 60;
    diff = (diff - ss) / 60;
    const mm = diff % 60;
    diff = (diff - mm) / 60;
    const hh = diff % 24;
    // let days = (diff - hh) / 24;

    return hh + ':' + mm + ':' + ss;

}

getTimeDiffInMillis(d) {
  const dd = new Date().getTime();

}

addCall(call) {

}
  addAlreadyActiveCalls(calls) {
    console.log(calls);
    for (let i = 0; i < calls.call.length; i++) {
      const obj: any = {};
      obj.name = calls.call[i].remoteParty.name;
      obj.number = calls.call[i].remoteParty.userDN.value;
      obj.callId = calls.call[i].callId;
      const d = new Date(calls.call[i].answerTime);

      // let diff = (d1.getTime() - d.getTime())/1000;

      // diff /= 60;

      obj.timerfun = Observable.interval(1000).subscribe(val => {
        obj.duration = new Date().getTime() - d.getTime();
        // console.log(obj.duration)
      }, error => {
        console.log(error);
      });

      // console.log(d.toLocaleTimeString());
      // let minutes = Math.floor(calls.call[i].answerTime / 60000);
      // let seconds = ((calls.call[i].answerTime % 60000) / 1000).toFixed(0);
      // let time = "" ;
      // console.log(minutes);
      // console.log(seconds);
      // if(parseInt(seconds) < 10){
      //   time = minutes + ':' + '0' + seconds;
      // }
      // else
      // {
      //   time = minutes + ':' + seconds;
      // }
      // obj.duration = time;
      obj.recording = 'NA';
      this.current.push(obj);
    }
  }
  addToActiveCalls (call) {
    console.log(call);
    const cl = this.current.filter(x => x.callId === call.callId);
    if (cl.length === 0) {
      console.log('Adding current call in current');
      console.log(call);
      const obj: any = {};
      obj.callId = call.callId;
      obj.name = 'DONNO';
      obj.number = call.remoteParty.address.value;

      const d = new Date(call.startTime);
      console.log(d);
      obj.timerfun = Observable.interval(1000).subscribe(val => {
        obj.duration = new Date().getTime() - d.getTime();
        console.log(obj.duration);
      }, error => {
        console.log(error);
      });
      obj.recording = 'NA';
      console.log(obj);
      this.current.push(obj);
    }
  }

  removeFromActiveCalls(call) {
    for (let i = 0; i < this.current.length; i++) {
      console.log(this.current[i].callId);
      console.log(call.callId);
      if (this.current[i].callId === call.callId) {
        console.log('Removing fromn current array');
        this.current[i].timerfun.unsubscribe(success => {
          console.log('Unsibscribed');

        });
        this.current.splice(i, 1);
        break;
      }
    }
  }
  callEventListner = (msg) => {
    // console.log('call event=' + msg);
    console.log(msg.call);
    console.log(msg.call.state);
    switch (msg.call.state) {
      case 'ACTIVE' : console.log('Call is active for call id ' + msg.call.callId);
                      this.addToActiveCalls(msg.call);
                      break;
      case 'ALERTING' : console.log('Incoming call for call id ' + msg.call.callId);
                        console.log(msg.call);
                        const cl1 = this.current.filter(x => x.callId === msg.call.callId);

                        if (msg.call.personality === 'TERMINATOR') {
                          console.log('From Remote party');
                        } else {
                          console.log('Incoming after dial');
                        }
                        this.addToActiveCalls(msg.call);
                        break;
      case 'HELD' : console.log('Call held by you for call id ' + msg.call.callId);
                    break;
      case 'RELEASED' : console.log('Call is released for call id ' + msg.call.callId);
                        // if present in the current calls array
                        // remove from current calls
                        this.removeFromActiveCalls(msg.call);
                        break;
      case 'REMOTE_HELD' : console.log('Call is held by remote party for call id ' + msg.call.callId);
                            break;
    }
  }

  hookStatusListner = (msg) => {
    console.log('hook status callback: msg=' + msg);
  }

}
