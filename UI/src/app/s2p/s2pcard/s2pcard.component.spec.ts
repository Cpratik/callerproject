import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S2pcardComponent } from './s2pcard.component';

describe('S2pcardComponent', () => {
  let component: S2pcardComponent;
  let fixture: ComponentFixture<S2pcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S2pcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S2pcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
