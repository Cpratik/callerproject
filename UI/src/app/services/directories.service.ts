import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DirectoriesService {

  constructor(private http: HttpClient) { }

  headerOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': localStorage.getItem('auth')
    })
  };
  getgroup() {
    return this.http.get(environment.baseurl + '/directories/' + localStorage.getItem('userid') + '/directories/group', this.headerOptions);
  }

  getpersonal() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(environment.baseurl + '/directories/' + localStorage.getItem('userid') + '/directories/personal', this.headerOptions);
  }

  postpersonal(personaldetails) {
    // tslint:disable-next-line: max-line-length
    return this.http.post(environment.baseurl + '/directories/' + localStorage.getItem('userid') + '/directories/personal', personaldetails, this.headerOptions);
  }

  getenterprise() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(environment.baseurl + '/directories/' + localStorage.getItem('userid') + '/directories/enterprise', this.headerOptions);
  }

  getcalllogs() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(environment.baseurl + '/directories/' + localStorage.getItem('userid') + '/directories/calllog', this.headerOptions);
  }
}
