import { TestBed } from '@angular/core/testing';

import { CalleventsService } from './callevents.service';

describe('CalleventsService', () => {
  let service: CalleventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalleventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
