import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalleventsService {

  constructor(private http: HttpClient) { }

  headerOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': localStorage.getItem('auth')
    })
  };

  login() {
    return this.http.post(environment.baseurl + '/calls/edslogin', {});
  }

  callevents() {
    return this.http.get(environment.baseurl + '/calls/callevent');
  }

  new(num) {
    console.log(environment.baseurl + '/calls/' + localStorage.getItem('userid') + '/calls/new');
    console.log(num);
    return this.http.post(environment.baseurl + '/calls/' + localStorage.getItem('userid') + '/calls/new', num, this.headerOptions);
  }

  hold(callid) {
    return this.http.put(environment.baseurl + '/calls/' + localStorage.getItem('userid') + '/calls/' + callid + 'hold', {}, this.headerOptions);
  }

  talk(callid) {
    return this.http.put(environment.baseurl + '/calls/' + localStorage.getItem('userid') + '/calls/' + callid + 'talk', {}, this.headerOptions);
  }
}
