import { Component, OnInit, Sanitizer } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
// import { DomSanitizer } from '@angular/platform-browser';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(private router: Router) { }

  // cssUrl = 'https://staśckpath.bootstrapcdn.com/bootswatch/4.4.1/cyborg/bootstrap.min.css';

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
