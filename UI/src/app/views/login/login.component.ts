import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {CallerService} from '../../caller.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})

export class LoginComponent {
  constructor(private authser: AuthService, private router: Router, private callerService: CallerService) {

  }
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.minLength(4), Validators.required]),
    password: new FormControl('', [Validators.minLength(4), Validators.required])
  });

  login() {
    this.authser.login(this.loginForm.value).subscribe( success => {
      console.log(success); // get the login token and store in local storage along with userid
      localStorage.setItem('userid', this.loginForm.value.username);
      localStorage.setItem('auth', 'Basic ' + btoa( this.loginForm.value.username + ':' + this.loginForm.value.password));
      this.callerService.login(this.loginForm.value);
      this.router.navigateByUrl('/home');
    }, error => {
      console.log(error);
    });
    console.log(this.loginForm.value);
  }

}
