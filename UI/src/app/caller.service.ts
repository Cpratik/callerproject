import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { environment } from '../environments/environment';
import {HttpClient} from '@angular/common/http';
// import {EDSConnector} from '../assets/js/eds.js';
declare var EDSConnector: any;

@Injectable({
  providedIn: 'root'
})
export class CallerService {

  constructor(private socket: Socket, private http: HttpClient) { }
  edsConnector: any;
  news: any = [];
  getFromEvent() {

  }

  login(data) {
    const endPoint = environment.edsurl;
    // var userCredentials = {username:"5128067815@nat.ipitimi.net", password:"$Sdixit@123", applicationid:"clientappname"};
    const userCredentials = {username: data.username, password: data.password, applicationid: 'VoipXP'};
    this.edsConnector = new EDSConnector(endPoint, userCredentials, null);

  }

  registerHandlers(loginSuccessListner, loginFailedListner, initialEventListner, callEventListner, hookStatusListner) {
    if (this.edsConnector) {
      this.edsConnector.registerLoginSuccessCallback(loginSuccessListner);
      this.edsConnector.registerLoginFailureCallback(loginFailedListner);
      this.edsConnector.registerInitialUpdateCallback(initialEventListner);
      this.edsConnector.registerCallUpdateCallback(callEventListner);
      this.edsConnector.registerHookStatusCallback(hookStatusListner);
      this.edsConnector.loginToEds();
    }
  }

   loginSuccessListner =  function() {
    console.log('application login success ');
    console.log('*************');
};

 loginFailedListner = function() {
    console.log('application login failed');
};

 initialEventListner = function(msg) {
    console.log(msg);
    console.log('initial login event=' + msg);
};

 callEventListner = function(msg) {
    console.log('call event=' + msg);
};

 hookStatusListner = function(msg) {
    console.log('hook status callback: msg=' + msg);
};

}
