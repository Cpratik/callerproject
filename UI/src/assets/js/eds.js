/**
	1. call the contructor providing required details.
	2. registed callback handlers using regist methods.
	3. call loginToEds method to set up eds connection. After this the registered callbacks will be called on respective events.
**/
class EDSConnector {
	
	/*
		edsEndPoint: eds host to connect to.
		userCredentails: user credentials to login
		callbackmap: map of callbacks for specific events.
	*/
	constructor(edsEndPoint, userCredentails, callbackmap){
		console.log("EDSConnector: constructor");
		this.edsEndPoint = edsEndPoint;
		this.userCredentails = userCredentails;
		this.callbackmap = callbackmap;
		
		this.EDS_EVENT_SOURCE_TYPES = {
			EDS_SOURCE: "eds",
			BROADWORKS_SOURCE: "broadworks",
			LOCAL_CLIENT_SOURCE: "localclient"
		}
	
		this.EDS_CONNECTOR_CONSTANT = {
			CONST_ENDPOINT: "endpoint",
			CONST_USERNAME: "username",
			CONST_PASSWORD: "password",
			CONST_APPLICATIONID: "applicationid",
			CONST_CLIENTSESSION_TOKEN: "token",
			CONST_NAME: "name",
			CONST_VALUE: "value",
			CONST_COMMAND: "command",
			CONST_SUCCESS: "success",
			CONST_FAILURE: "failure",
			CONST_ERROR:"error",
			CONST_ALREADY_LOGGEDIN: "alreadyloggedin",
			CONST_NO_MULTIPLE_TAB_SUPPORT: "nomultipletabsupport",
			CONST_FAILURE_NO_CONNECTION: "noconnection",
			CONST_ERROR_CONNECTING: "errorconnecting",
			CONST_CONNECTION_CLOSED: "connectionclosed",
			CONST_INVALID_LOGIN_PARAMETER: "invalidloginparam",
			
			CONST_CMD_LOGIN: "login",
			CONST_CMD_LOGOUT: "logout",		
			CONST_CMD_LOGIN_STATUS: "loginstatus",
			CONST_CMD_ALIVE: "alive"
			
		};
		
		this.SUCCESS_LOGGEDIN_CB = "SUCCESS_LOGGEDIN_CB";
		this.ERROR_LOGIN_CB = "ERROR_LOGIN_CB";
		this.INITIALUPDATE_CB= "INITIALUPDATE_CB";
		this.CALLUPDATE_CB= "CALLUPDATE_CB";
		this.HOOKSTATUS_CB = "HOOKSTATUS_CB";
	
		
		this.CALLBACK_MAP = {
			SUCCESS_LOGGEDIN_CB: [],
			ERROR_LOGIN_CB: [],
			INITIALUPDATE_CB: [],
			CALLUPDATE_CB: [],
			HOOKSTATUS_CB: []
		};
		
		this.CALLBACK_MAP[this.SUCCESS_LOGGEDIN_CB].splice();
		this.CALLBACK_MAP[this.ERROR_LOGIN_CB].splice();
		this.CALLBACK_MAP[this.INITIALUPDATE_CB].splice();
		this.CALLBACK_MAP[this.CALLUPDATE_CB].splice();
		this.CALLBACK_MAP[this.HOOKSTATUS_CB].splice();
		
		this.edsWebSocket = null;
		this.edsSessionId = null;
		this.aliveTimerHandle = null;
		var selfClass = this;
		var aliveTimerHandler = selfClass.sendAliveMessage;
		/****     Hnadling of web socket specific events.       ****/	
		this.onWebSocketConnectSuccess = function() {
			selfClass.log("onWebSocketConnectSuccess: web socket connection success, starting login process");
			selfClass.startLoginProcess();
		};
		
		this.onWebSocketClose = function() {
			 selfClass.log("onWebSocketClose: web socket connection closed, need to cleanup everything");
			 selfClass.edsSessionId = null;
			 selfClass.handleWebSocketClose();
		};
		
		this.onWebSocketMessage = function(message) {
			 selfClass.log("onWebSocketMessage:message" + message);
			try {
				 selfClass.handleServerMessage(message.data);
			} catch (e) {
				window.alert("Unable to handle message:" + message);
				console.log("onWebSocketMessage: exception: " + e);
				window.alert("onWebSocketMessage: exception: " + e);
			}
		};

		
	}
	
	log(msg) {
		// console.log("EDSConnector::EDSLOG-MSG:"+msg);
	};
	
	/* public method to login to eds. ensure that all callbacks are registered before calling this method */
	loginToEds(){
		var self = this;
		self.log("loginToEds: start");
		if(this.edsSessionId == null) {
			this.edsSessionId = self.generateUUID();
		} else {
			alert("login to eds already completed, cannot login again");
		}
		this.edsWebSocket = new WebSocket(this.edsEndPoint);
		this.edsWebSocket.onopen = this.onWebSocketConnectSuccess;
		this.edsWebSocket.onclose = this.onWebSocketClose;
		this.edsWebSocket.onmessage = this.onWebSocketMessage;
	}

	/* start login to EDS, called when websocket connection is success */
	startLoginProcess() {	
		var self = this;
		console.log("startlogin process");
		self.log("startLoginProcess: sending eds login command");
		var loginCommand = self.getLoginCommand();
		self.sendMessage(JSON.stringify(loginCommand));	
	}
	
		/** This method handles application call control events, service events ***/	
	handleBwksEvents(eventObj){
		var self = this;
		self.log("handleBwksEvents: eventObj = " + eventObj);
		var command = eventObj.Command;
		var eventData = eventObj.eventData;
		var tempIndex = 0;
		var currentCBId = 0;
	    self.log("handleBwksEvents: command = "+command);
		self.log("handleBwksEvents: eventData = "+eventData);
		switch (command) {
			case "CallSubscriptionEvent"://On Successful call subscription we get this event. Using it as successful
				self.notifyAllCallbacks(this.INITIALUPDATE_CB,eventData);
				break;
			case "presencemodified":
				break;
			case "CallEvent":
				self.notifyAllCallbacks(this.CALLUPDATE_CB,eventData);
				break;
			case "HookStatusEvent":
				//Currently not handling it. Not needed.
				//if(onCallUpdateCB!=null)
				//	onCallUpdateCB(eventObj.eventData);
				break;
			case "error":
				//showError(result);
				break;
			default:
				alert("Unhandled Message");
				break;
		}	
	}
	
	/* called for all event source of type EDS_EVENT_SOURCE_TYPES.EDS_SOURCE. Called for logins*/	
	handleEdsEvents(eventObj){
		var self = this;
		self.log("handleEdsEvents: eventObj=" + eventObj);
		self.log("handleEdsEvents: command=" + eventObj.Command);
		var command = eventObj.Command;
		switch (command) {
			case this.EDS_CONNECTOR_CONSTANT.CONST_CMD_LOGIN_STATUS:
				var loginResult = self.parseEventLoginStatus(eventObj)
				if(loginResult===this.EDS_CONNECTOR_CONSTANT.CONST_SUCCESS) {
				}else {
					logoutEds();					
				}
				self.notifyAllCallbacks(this.SUCCESS_LOGGEDIN_CB,loginResult);
				
				if( loginResult === this.EDS_CONNECTOR_CONSTANT.CONST_SUCCESS){
					this.aliveTimerHandle = setInterval(this.aliveTimerHandler, 1200);
				}
				break;
				
			case this.EDS_CONNECTOR_CONSTANT.CONST_CMD_ALIVE:
				//maybe we can handle something here...By keeping count or rather do this on server to keep references
				break;
				
			case this.EDS_CONNECTOR_CONSTANT.CONST_ALREADY_LOGGEDIN:
				alert("User already loggedin at another location");
				break;
				
			case this.EDS_CONNECTOR_CONSTANT.CONST_NO_MULTIPLE_TAB_SUPPORT:
				alert("Multiple tabs not supported");
				break;	
			default:
				alert();
		}
	}
	
		/* parse event login status and return success or failure status of login */
	parseEventLoginStatus(eventObj) {
		var self = this;
		var loginResult = this.EDS_CONNECTOR_CONSTANT.CONST_FAILURE;
		var index = 0;
		for(index=0; index < eventObj.Data.length; index++){
			if(eventObj.Data[index].Name==="status"){
				if(eventObj.Data[index].Value==="success"){
					loginResult = this.EDS_CONNECTOR_CONSTANT.CONST_SUCCESS;
					break;
				} 
			}	    		
		}
		return loginResult;
		
	}
	
	getLoginCommand() {
		var self = this;
		var loginData = {};
		loginData.Command = this.EDS_CONNECTOR_CONSTANT.CONST_CMD_LOGIN;
		loginData.Data = [];
	 
		loginData.Data[0] = new Object();
		loginData.Data[0].Name = this.EDS_CONNECTOR_CONSTANT.CONST_USERNAME;
		loginData.Data[0].Value = this.userCredentails.username;

		loginData.Data[1] = new Object();
		loginData.Data[1].Name = this.EDS_CONNECTOR_CONSTANT.CONST_PASSWORD;
		loginData.Data[1].Value = this.userCredentails.password;

		loginData.Data[2] = new Object();
		loginData.Data[2].Name = this.EDS_CONNECTOR_CONSTANT.CONST_APPLICATIONID;
		loginData.Data[2].Value = this.userCredentails.applicationid;
		
		loginData.Data[3] = new Object();
		loginData.Data[3].Name = this.EDS_CONNECTOR_CONSTANT.CONST_CLIENTSESSION_TOKEN;
		loginData.Data[3].Value = this.edsSessionId;	
		return loginData;
	}

	/**** Handling of callback registrations and notifications ****/
	registerLoginSuccessCallback(loginSuccessCallback) {
		var self = this;
		self.log("registerLoginSuccessCallback");
		//console.log(this.CALLBACK_MAP);
		if(loginSuccessCallback!==null) {
			this.CALLBACK_MAP[this.SUCCESS_LOGGEDIN_CB].push(loginSuccessCallback);
		}
	}
	
	registerLoginFailureCallback(loginFailureCallback) {
		if(loginFailureCallback!==null) {
			this.CALLBACK_MAP[this.ERROR_LOGIN_CB].push(loginFailureCallback);
		}
	}

	registerInitialUpdateCallback(initialCallback) {
		if(initialCallback!==null) {
			this.CALLBACK_MAP[this.INITIALUPDATE_CB].push(initialCallback);
		}
	}
	
	registerCallUpdateCallback(callUpdateCallback) {
		if(callUpdateCallback!==null) {
			this.CALLBACK_MAP[this.CALLUPDATE_CB].push(callUpdateCallback);
		}
	}

	registerHookStatusCallback(hookStatusCallback) {
		if(hookStatusCallback!==null) {
			this.CALLBACK_MAP[this.HOOKSTATUS_CB].push(hookStatusCallback);
		}
	}
	
	
	/* All calback listners of type callBackKey are notified with msgForCallback. The callbacks of type callBackKey are registered in this.CALLBACK_MAP*/
	notifyAllCallbacks(callBackKey, msgForCallback) {
		var self = this;
		self.log("notifyAllCallbacks: callbackkey=" + callBackKey + " msgForCallback="+msgForCallback);
		var callBacks = this.CALLBACK_MAP[callBackKey];
		self.log("notifyAllCallbacks: callBacks=" + callBacks);
		for( var tempIndex = 0 ; tempIndex < callBacks.length; tempIndex++) {
			if(callBacks[tempIndex] !== null) {
				try {
					callBacks[tempIndex](msgForCallback);
				} catch(e){
					self.log("notifyAllCallbacks: ERROR: callbackkey=" + callBackKey + " msgForCallback="+msgForCallback + " tempIndex=" + tempIndex);
					callBacks.splice(tempIndex,1);
					tempIndex--;
				}
			}
		}
	}
	
	
	/* helper method*/
	generateUUID() {
		var d = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
				function(c) {
					var r = (d + Math.random() * 16) % 16 | 0;
					d = Math.floor(d / 16);
					return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
				});
		return uuid;
	}




		


		/* called when an event is recieved from server over websocket, called from onWebSocketMessage*/
	handleServerMessage(message){
		
		var self = this;
		self.log("received server message="+ message);
		var eventObj = JSON.parse(message);
		var source = eventObj.eventSource;
		switch (source) {
			case this.EDS_EVENT_SOURCE_TYPES.EDS_SOURCE:
			//"eds"://On Successful call subscription we get this event. Using it as successful
				self.handleEdsEvents(eventObj);
				break;
			case this.EDS_EVENT_SOURCE_TYPES.BROADWORKS_SOURCE :
				self.handleBwksEvents(eventObj);
				break;
			case this.EDS_EVENT_SOURCE_TYPES.LOCAL_CLIENT_SOURCE  : 
				//For now there is only an error... Like the endpoint is not reachable
			default:
				alert("Unhandled Event Source");
				break;
		}
	};

	/* send the message to eds server over web socket. */
	sendMessage(message){
		var self = this;
		self.log("sending message="+message);
		try{
			if(this.edsWebSocket!==null) {
				this.edsWebSocket.send(message);
			}
		}catch(e){
			self.log("sendMessage: error sending message"+e);
			self.stopAliveTimer();
			this.edsWebSocket = null;
		}
	};	
	
	
	/******   Keep alive handling ******/
	sendAliveMessage(){
		var self = this;
		if(this.edsWebSocket != null) {
			try{
				var aliveData = {};
				aliveData.Command = EDS_CONNECTOR_CONSTANT.CONST_CMD_ALIVE;
				
				aliveData.Data = [];
				aliveData.Data[0] = new Object();
				aliveData.Data[0].Name = EDS_CONNECTOR_CONSTANT.CONST_USERNAME;
				aliveData.Data[0].Value = this.userCredentails.username;;

				aliveData.Data[1] = new Object();
				aliveData.Data[1].Name = EDS_CONNECTOR_CONSTANT.CONST_PASSWORD;
				aliveData.Data[1].Value = this.userCredentails.password;
				
				aliveData.Data[2] = new Object();
				aliveData.Data[2].Name = EDS_CONNECTOR_CONSTANT.CONST_APPLICATIONID;
				aliveData.Data[2].Value = this.userCredentails.applicationid;
				
				self.sendMessage(JSON.stringify(aliveData));
			}catch(e){
				self.stopAliveTimer();
			}
		}
		else
			self.stopAliveTimer();
	};

	stopAliveTimer() {
		if(this.aliveTimerHandle!==null) {
			clearInterval(this.aliveTimerHandle);
			this.aliveTimerHandle = null;
			this.edsWebSocket = null;
			//localStorage.clear();
		}
	};
	


}